/*
S33 Activity:
3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
11. Update a to do list item by changing the status to complete and add a date when the status was changed.
12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/

//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => {

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});


//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.	
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json()) 
.then((json) => {
	console.log(json);

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.	
	console.log('The item "'  + json.title + '" on the list has status of ' + json.completed);
});

//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "Created to do list item",
			userId:1,
			completed:"false"
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
/*
9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/
		title: "Updated to do list Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "pending",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))



//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
//11. Update a to do list item by changing the status to complete and add a date when the status was changed.
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete"
	})
})
.then(response => response.json())
.then(json => console.log(json))

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	}	
).then(response => response.json())
.then(json => console.log(json))

